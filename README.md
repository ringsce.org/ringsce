[![Gitter](https://badges.gitter.im/plvicente-ringsce/community.svg)](https://gitter.im/plvicente-ringsce/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)
ringsce is an ide for big sur+ (macos 11+). It's being built on the top of C++ and swiftUI. We are trying to build all the projects with cmake.
We hope to deliver ringsce to all users, whom use XCode or appcode. This is not an appcode replacement. It was there a niche. We are here to exploit it.
Today, the ide are still in use. There are so much to do.

Donations:
Click on the sponsor buttons.

Contributions:
If you want to learn swift, c++ and qt even cmake. You are on the right place. Just send me an email or try to use discord
User : plvicente

Communities
Discord & gitter.

Releases: Not yet. This is a breeze version. Too much on the beginning. 
 
![Screenshot](RINGSCE_v2.png)

[![paypal](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](pdvicente@gleentech.com)

