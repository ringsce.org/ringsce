#-------------------------------------------------
#
# Project created by QtCreator 2019-04-02T13:12:29
#
#-------------------------------------------------

QT       += core gui widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RINGSCEIDE
TEMPLATE = app

qtHaveModule(printsupport): QT += printsupport
requires(qtConfig(fontdialog))

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG+= c++11 sdk_no_version_check
SOURCES += \
        main.cpp \
    highlighter.cpp \
    mainwindow.cpp \
    new_file.cpp \
    settings.cpp \
    about.cpp \
    update.cpp \
    new_project.cpp \
    splashscreen.cpp \
    notepad.cpp

HEADERS += \
    highlighter.h \
    mainwindow.h \
    myThreads.h \
    splashscreen.h \
    ui_new_file.h \
    ui_new_project.h \
    ui_below_stack.h \
    notepad.h

FORMS += \
    mainwindow.ui \
    new_file.ui \
    new_project.ui \
    notepad.ui \
    preferences.ui \
    templates.ui

RESOURCES += qdarkstyle/style.qrc \
             notepad.qrc


DISTFILES += \
    README.md \
    RINGSCE_v2.png \
    app.py \
    requirements.txt \
    syntax.py

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../Projects/RingsCELib/release/ -lRingsCELib.1.0.0
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../Projects/RingsCELib/debug/ -lRingsCELib.1.0.0
else:unix: LIBS += -L$$PWD/../../../Projects/RingsCELib/ -lRingsCELib.1.0.0

INCLUDEPATH += $$PWD/../../../Projects/RingsCELib
DEPENDPATH += $$PWD/../../../Projects/RingsCELib
