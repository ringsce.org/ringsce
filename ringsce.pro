QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17 sdk_no_version_check

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    mainwindow.cpp \

HEADERS += \
    mainwindow.h

FORMS += \
    mainwindow.ui \

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    CMakeLists.txt \
    License.txt \
    README.md \
    RINGSCE_v2.png \
    latex/doxygen.sty \
    latex/longtable_doxygen.sty \
    latex/md__r_e_a_d_m_e.tex \
    latex/refman.tex \
    latex/tabu_doxygen.sty \
    make-macos.sh \
    scripts/generate-dmg.sh \
    scripts/update.sh \
    tools/QtConsole/LICENSE \
    tools/QtConsole/README.md \
    tools/QtConsole/qtconsole.pri \
    tools/django.sh \
    tools/qtdesigner.sh \
    tools/ruby.sh \
    welcome/index.html

SUBDIRS += \

RESOURCES += \
    qdarkstyle/style.qrc
